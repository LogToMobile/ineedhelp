package com.logtomobile.ineedhelp;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.logtomobile.ineedhelp.maps.Route;
import com.logtomobile.ineedhelp.maps.Routing;
import com.logtomobile.ineedhelp.maps.RoutingListener;
import com.logtomobile.ineedhelp.net.HttpPost;
import com.logtomobile.ineedhelp.net.OnRequestFinishedListener;
import com.logtomobile.ineedhelp.net.RequestExecutor;

import java.io.IOException;
import java.util.List;

public class AmbulanceActivity extends FragmentActivity implements RoutingListener {
    protected GoogleMap map;
    protected LatLng start;
    protected LatLng end;
    private double mLatitude;
    private double mLongitude;
    private Routing mRouting;
    private boolean mIsTaskRunning;
    private RequestExecutor mRequestExecutor = new RequestExecutor();

    private Button mBtnFind;
    private Button mBtnNavigate;
    private EditText mEdtFind;

    private void findViews() {
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        map = fm.getMap();

        mBtnFind = (Button) findViewById(R.id.btnFind);
        mBtnNavigate = (Button) findViewById(R.id.btnNavigate);
        mEdtFind = (EditText) findViewById(R.id.edtFind);
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mEdtFind.getWindowToken(), 0);
    }

    private void setListeners() {
        mBtnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(mEdtFind.getText())) {
                    if (!mIsTaskRunning) {
                        mBtnNavigate.setEnabled(false);
                        LatLng position = getLocationFromAddress(mEdtFind.getText().toString());
                        if (position != null) {
                            map.clear();

                            if (mLatitude != 0.0 && mLongitude != 0.0) {
                                start = new LatLng(mLatitude, mLongitude);
                                end = position;
                                mIsTaskRunning = true;
                                mRouting.execute(start, end);
                            } else {
                                Toast.makeText(getApplicationContext(), "Cannot recognize your position!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Cannot find location!", Toast.LENGTH_SHORT).show();
                        }
                        hideKeyboard();
                    } else {
                        Toast.makeText(getApplicationContext(), "Task is already running", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mEdtFind.setError("Address cannot be empty!");
                }

            }
        });

        mBtnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("geo:"+start.latitude+","+start.longitude+"?q="+end.latitude+","+end.longitude);
                showMap(uri);
            }
        });
    }

    private void updateLocation() {
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mLatitude = location.getLatitude();
                mLongitude = location.getLongitude();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    /**
     * This activity loads a map and then displays the route and pushpins on it.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);
        findViews();
        setListeners();
        updateLocation();


        mRouting = new Routing(Routing.TravelMode.DRIVING);
        mRouting.registerListener(this);
    }

    public LatLng getLocationFromAddress(String strAddress) {
        Geocoder geocoder = new Geocoder(this);
        List<Address> addresses;
        LatLng latLng;

        try {
            addresses = geocoder.getFromLocationName(strAddress, 5);

            if (addresses == null || addresses.isEmpty()) {
                return null;
            }

            Address location = addresses.get(0);
            latLng = new LatLng((location.getLatitude()),
                    (location.getLongitude()));

            return latLng;
        } catch (IOException e) {
            return null;
        }
    }

    private void showMap(Uri geoLocation) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private String getPhoneId() {
        String identifier = null;

        TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null)
            identifier = tm.getDeviceId();
        if (identifier == null || TextUtils.isEmpty(identifier))
            identifier = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        return identifier;
    }

    private void sendPostOnServer() {
        final OnRequestFinishedListener<String> listener = new OnRequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(int responseCode, String message, boolean canceled) {
            }
        };

        final ContentValues contentValues = new ContentValues();
        contentValues.put("action", "add_map_road");
        contentValues.put("src_lat", "" + start.latitude);
        contentValues.put("src_lng", "" + start.longitude);
        contentValues.put("dest_lat", "" + end.latitude);
        contentValues.put("dest_lng", "" + end.longitude);
        contentValues.put("device_id", getPhoneId());

        HttpPost<String> postRequest = new HttpPost<String>(
                mRequestExecutor,
                false,
                getResources().getString(R.string.server_host),
                contentValues,
                new HttpPost.ServerResponseParser<String>() {
                    @Override
                    public String parse(String message) {
                        return message;
                    }
                },
                listener
        );

        postRequest.execute();
    }


    @Override
    public void onRoutingFailure() {
        mIsTaskRunning = false;
        mRouting = new Routing(Routing.TravelMode.DRIVING);
        mRouting.registerListener(this);
    }

    @Override
    public void onRoutingStart() {
        // The Routing Request starts
    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
        PolylineOptions polyoptions = new PolylineOptions();
        polyoptions.color(Color.BLUE);
        polyoptions.width(5);
        polyoptions.addAll(mPolyOptions.getPoints());
        map.addPolyline(polyoptions);

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));
        map.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));
        map.addMarker(options);

        final CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(end.latitude, end.longitude));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

        map.animateCamera(zoom, 500, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                map.moveCamera(center);
            }

            @Override
            public void onCancel() {

            }
        });

        mIsTaskRunning = false;
        mRouting = new Routing(Routing.TravelMode.DRIVING);
        mRouting.registerListener(this);

        mBtnNavigate.setEnabled(true);
        sendPostOnServer();
    }
}
