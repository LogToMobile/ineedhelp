package com.logtomobile.ineedhelp;

import android.app.Application;
import android.content.Context;

import com.google.common.eventbus.EventBus;
import com.logtomobile.ineedhelp.service.INeedHelpProviderService;

import java.util.HashMap;

/**
 *@author Tomasz Trybała
 */
public class INeedHelpApplication extends Application {
    private static Context sContext;
    private static EventBus sBus;
    private static HashMap<Integer, INeedHelpProviderService.HelloAccessoryProviderConnection> mApplicationConnectionMap;
    private static int mConnectionId;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;
        sBus = new EventBus("Sound Pranks Event Bus");
    }

    /**
     * Returns application context;
     *
     * @return context
     */
    public static Context getAppContext() {
        return sContext;
    }

    /**
     * Returns application event bus.
     *
     * @return application event bus
     */
    public static EventBus getApplicationEventBus() {
        return sBus;
    }

    public static HashMap<Integer, INeedHelpProviderService.HelloAccessoryProviderConnection> getMap(){
        return mApplicationConnectionMap;
    }

    public static void setMap(HashMap<Integer, INeedHelpProviderService.HelloAccessoryProviderConnection> map){
        mApplicationConnectionMap = map;
    }

    public static int getId(){
        return mConnectionId;
    }

    public static void setId(int id){
        mConnectionId = id;
    }
}
