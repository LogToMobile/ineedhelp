package com.logtomobile.ineedhelp.maps;

public interface Parser {
    public Route parse();
}
