package com.logtomobile.ineedhelp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import static com.logtomobile.ineedhelp.BundleConstants.ITEM_1;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_2;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_3;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_4;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_5;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_6;

/**
 *@author Tomasz Trybała
 */
public class SafetyAdvicesActivity extends Activity {
    private LinearLayout mLl1;
    private LinearLayout mLl2;
    private LinearLayout mLl3;
    private LinearLayout mLl4;
    private LinearLayout mLl5;
    private LinearLayout mLl6;

    private void findViews(){
        mLl1 = (LinearLayout) findViewById(R.id.ll1);
        mLl2 = (LinearLayout) findViewById(R.id.ll2);
        mLl3 = (LinearLayout) findViewById(R.id.ll3);
        mLl4 = (LinearLayout) findViewById(R.id.ll4);
        mLl5 = (LinearLayout) findViewById(R.id.ll5);
        mLl6 = (LinearLayout) findViewById(R.id.ll6);
    }

    private void setClickAction(){
        mLl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafetyAdvicesActivity.this, AdviceActivity.class);
                intent.putExtra(ITEM_1, ITEM_1);
                startActivity(intent);
            }
        });

        mLl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafetyAdvicesActivity.this, AdviceActivity.class);
                intent.putExtra(ITEM_2, ITEM_2);
                startActivity(intent);
            }
        });

        mLl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafetyAdvicesActivity.this, AdviceActivity.class);
                intent.putExtra(ITEM_3, ITEM_3);
                startActivity(intent);
            }
        });

        mLl4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafetyAdvicesActivity.this, AdviceActivity.class);
                intent.putExtra(ITEM_4, ITEM_4);
                startActivity(intent);
            }
        });

        mLl5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafetyAdvicesActivity.this, AdviceActivity.class);
                intent.putExtra(ITEM_5, ITEM_5);
                startActivity(intent);
            }
        });

        mLl6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafetyAdvicesActivity.this, AdviceActivity.class);
                intent.putExtra(ITEM_6, ITEM_6);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        findViews();
        setClickAction();
    }
}
