package com.logtomobile.ineedhelp.event;

/**
 *@author Tomasz Trybała
 */
public class SendEvent {
    private String mMessage;

    public SendEvent(String message){
        mMessage = message;
    }

    public String getMessage(){return mMessage;}
}
