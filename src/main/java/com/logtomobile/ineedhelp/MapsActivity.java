package com.logtomobile.ineedhelp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private double mLat = 0.0;
    private double mLng = 0.0;
    private double mSrcLat = 0.0;
    private double mSrcLng = 0.0;
    private double mDestLat = 0.0;
    private double mDestLng = 0.0;
    private String mCategory = "";
    private String mNote = "";
    private String mPeople = "";
    private String mPolice = "";
    private String mType = "";
    private double[] mPoints;
    private double[] mSteps;
    private TextView mTxtvNote;
    private TextView mTxtvError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        findViews();
        setUpMap(getIntent());
    }

    private void findViews(){
        mTxtvNote = (TextView) findViewById(R.id.txtvNote);
        mTxtvError = (TextView) findViewById(R.id.txtvError);
    }

    private void loadFromIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();

            if (extras.containsKey("lat")) {
                mLat = extras.getDouble("lat");
            }

            if (extras.containsKey("lng")) {
                mLng = extras.getDouble("lng");
            }

            if (extras.containsKey("src_lat")) {
                mSrcLat = extras.getDouble("src_lat");
            }

            if (extras.containsKey("src_lng")) {
                mSrcLng = extras.getDouble("src_lng");
            }

            if (extras.containsKey("dest_lat")) {
                mDestLat = extras.getDouble("dest_lat");
            }

            if (extras.containsKey("dest_lng")) {
                mDestLng = extras.getDouble("dest_lng");
            }

            if (extras.containsKey("points")) {
                mPoints = extras.getDoubleArray("points");
            } else {
                mPoints = new double[]{};
            }

            if (extras.containsKey("steps")) {
                mSteps = extras.getDoubleArray("steps");
            } else {
                mSteps = new double[]{};
            }

            if (extras.containsKey("note")) {
                mNote = extras.getString("note");
            } else {
                mNote = "";
            }

            if (extras.containsKey("category")) {
                mCategory = extras.getString("category");
            } else {
                mCategory = "";
            }

            if (extras.containsKey("type")) {
                mType = extras.getString("type");
            } else {
                mType = "";
            }

            if (extras.containsKey("people")) {
                mPeople = extras.getString("people");
            } else {
                mPeople = "";
            }

            if (extras.containsKey("police")) {
                mPolice = extras.getString("police");
            } else {
                mPolice = "";
            }
        }
    }

    private void setText(){
        String result = "";
        if(mNote != null && !mNote.equals("")){
            result += "\nNote: "+ mNote;
        }

        if(mType != null && !mType.equals("")){
            result += "\nType: "+ mType;
        }

        if(mPeople != null && !mPeople.equals("")){
            result += "\nPeople involved: "+ mPeople;
        }

        if(mPolice != null && mPolice.equals("true")){
            result += "\nPlease call the police";
        }

        if(result.length() > 2){
            result = result.substring(1, result.length());
        }
        mTxtvNote.setText(result);
    }

    private List getPoints(double[] points){
        ArrayList<LatLng> latLngs = new ArrayList<LatLng>();
        for (int i = 0; i < points.length; i = i + 2) {
            latLngs.add(new LatLng(points[i], points[i + 1]));
        }
        return latLngs;
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap(Intent intent) {
        mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                .getMap();

        if(mMap != null) {
            mMap.clear();
            mTxtvError.setVisibility(View.GONE);

            loadFromIntent(intent);

            if (getActionBar() != null) {
                getActionBar().setTitle(mCategory);
            }

            setText();

            if(mSteps != null && mSteps.length > 0) {
                PolylineOptions polylineOptions = new PolylineOptions();
                polylineOptions.color(Color.BLUE);
                polylineOptions.width(5);
                polylineOptions.addAll(getPoints(mSteps));
                mMap.addPolyline(polylineOptions);

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mSrcLat, mSrcLng)))
                .setIcon(BitmapDescriptorFactory
                .fromResource(R.drawable.ic_start));

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mDestLat, mDestLng)))
                        .setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_finish));

                LatLng latLng = new LatLng(mLat, mLng);
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(mCategory))
                        .setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_ambulance));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }else{
                LatLng latLng = new LatLng(mLat, mLng);
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(mCategory))
                        .setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_marker));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }

            PolylineOptions options = new PolylineOptions().geodesic(true);

            if (mPoints != null) {
                boolean wasFirst = false;
                LatLng lng = null;
                for (int i = 0; i < mPoints.length; i = i + 2) {
                    if (!wasFirst) {
                        wasFirst = true;
                        lng = new LatLng(mPoints[i], mPoints[i + 1]);
                    }
                    options.add(new LatLng(mPoints[i], mPoints[i + 1]));
                }

                if (lng != null) {
                    options.add(lng);
                }

                options.color(Color.RED);
                mMap.addPolyline(options);
            }
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        }else{
            mTxtvNote.setVisibility(View.GONE);
            mTxtvError.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setUpMap(intent);
    }
}
