package com.logtomobile.ineedhelp.service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;

import com.logtomobile.ineedhelp.INeedHelpApplication;
import com.logtomobile.ineedhelp.MapsActivity;
import com.logtomobile.ineedhelp.R;
import com.logtomobile.ineedhelp.net.HttpPost;
import com.logtomobile.ineedhelp.net.OnRequestFinishedListener;
import com.logtomobile.ineedhelp.net.RequestExecutor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Tomasz Trybała
 */
public class SendService extends Service {
    private static final String DEVICE_NAME = "deviceName";
    private static final String DATA_PREFS = "dataPrefs";
    private static final int TIME_DELAYED = 15 * 1000;//15sec
    private RequestExecutor mRequestExecutor = new RequestExecutor();
    private double mLatitude;
    private double mLongitude;
    private static final int CHANNEL_ID = 105;
    private final IBinder myBinder = new MyLocalBinder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public class MyLocalBinder extends Binder {
        public SendService getService() {
            return SendService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    sendPostOnServer();
                    try {
                        Thread.sleep(TIME_DELAYED);
                    } catch (Exception e) {
                        //
                    }
                }
            }
        };
        thread.start();

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mLatitude = location.getLatitude();
                mLongitude = location.getLongitude();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    private String getPhoneId() {
        String identifier = null;

        TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null)
            identifier = tm.getDeviceId();
        if (identifier == null || TextUtils.isEmpty(identifier))
            identifier = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        return identifier;
    }

    private void sendMessageToGear(final String message) {
        if (INeedHelpApplication.getMap() != null) {
            final INeedHelpProviderService.HelloAccessoryProviderConnection uHandler = INeedHelpApplication.getMap().get(Integer
                    .parseInt(String.valueOf(INeedHelpApplication.getId())));
            if (uHandler == null) {
                return;
            }

            new Thread(new Runnable() {
                public void run() {
                    try {
                        uHandler.send(CHANNEL_ID, message.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private void parseMessage(String message) {
        try {
            JsonReader reader = new JsonReader(new InputStreamReader(new ByteArrayInputStream(message.getBytes()), "UTF-8"));
            reader.beginObject();
            double lat = 0.0;
            double lng = 0.0;
            double src_lat = 0.0;
            double src_lng = 0.0;
            double dest_lat = 0.0;
            double dest_lng = 0.0;
            String category = "";
            String note = "";
            String type = "";
            String people = "";
            String police = "";
            double[] points = new double[]{};
            double[] steps = new double[]{};

            while (reader.hasNext()) {
                String name = reader.nextName();

                if ("lat".equals(name)) {
                    lat = reader.nextDouble();
                } else if ("lng".equals(name)) {
                    lng = reader.nextDouble();
                } else if ("src_lat".equals(name)) {
                    src_lat = reader.nextDouble();
                } else if ("src_lng".equals(name)) {
                    src_lng = reader.nextDouble();
                } else if ("dest_lat".equals(name)) {
                    dest_lat = reader.nextDouble();
                } else if ("dest_lng".equals(name)) {
                    dest_lng = reader.nextDouble();
                } else if ("category".equals(name)) {
                    category = reader.nextString();
                } else if ("note".equals(name)) {
                    note = reader.nextString();
                } else if ("type".equals(name)) {
                    type = reader.nextString();
                } else if ("peoplenumber".equals(name)) {
                    people = reader.nextString();
                } else if ("alertpolice".equals(name)) {
                    police = reader.nextString();
                } else if ("points".equals(name)) {
                    points = toArray(reader.nextString());
                } else if ("steps".equals(name)) {
                    steps = toArray(reader.nextString());
                } else {
                    reader.skipValue();
                }
            }

            reader.endObject();
            Intent intent = new Intent(getBaseContext(), MapsActivity.class);
            intent.putExtra("lat", lat);
            intent.putExtra("lng", lng);
            intent.putExtra("src_lat", src_lat);
            intent.putExtra("src_lng", src_lng);
            intent.putExtra("dest_lat", dest_lat);
            intent.putExtra("dest_lng", dest_lng);
            intent.putExtra("category", category);
            intent.putExtra("note", note);
            intent.putExtra("points", points);
            intent.putExtra("steps", steps);
            intent.putExtra("police", police);
            intent.putExtra("type", type);
            intent.putExtra("people", people);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplication().startActivity(intent);
        } catch (IOException ioe) {
            //json is empty
        }
    }

    private double[] toArray(String pattern) {
        pattern = pattern.replace("_", " ");
        String[] strings = pattern.split("\\s");
        double[] array = new double[strings.length];
        for (int i = 0; i < strings.length; i++) {
            array[i] = Double.parseDouble(strings[i]);
        }

        return array;
    }

    private String getPhoneName(){
        SharedPreferences prefs = getBaseContext().getSharedPreferences(DATA_PREFS, 0);

        return prefs.getString(DEVICE_NAME, "");
    }

    private void sendPostOnServer() {
        final OnRequestFinishedListener<String> listener = new OnRequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(int responseCode, String message, boolean canceled) {
                Log.e("dupa", "code: "+responseCode+", message: "+message);
                parseMessage(message);
                if (!message.equals("")) {
                    sendMessageToGear(message);
                }
            }
        };

        final ContentValues contentValues = new ContentValues();
        contentValues.put("action", "get_event"); //add_map_road
        contentValues.put("lat", "" + mLatitude); //src_lat src_lng
        contentValues.put("lng", "" + mLongitude);// dest_lat dest_lng
        contentValues.put("deviceid", getPhoneId());
        contentValues.put("name", getPhoneName());

        HttpPost<String> postRequest = new HttpPost<String>(
                mRequestExecutor,
                false,
                getResources().getString(R.string.server_host),
                contentValues,
                new HttpPost.ServerResponseParser<String>() {
                    @Override
                    public String parse(String message) {
                        return message;
                    }
                },
                listener
        );

        postRequest.execute();
    }
}
