package com.logtomobile.ineedhelp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

/**
 *@author Tomasz Trybała
 */
public class SettingsDialogFragment extends DialogFragment {
    private static final String DEVICE_NAME = "deviceName";
    private static final String DATA_PREFS = "dataPrefs";
    private EditText mEditText;

    private void loadData() {
        Context context = INeedHelpApplication.getAppContext();
        SharedPreferences prefs = context.getSharedPreferences(DATA_PREFS, 0);

        String name1 = prefs.getString(DEVICE_NAME, "");
        if (!name1.equals("")) {
            mEditText.setText(name1);
        }
    }

    private void saveData() {
        Context context = INeedHelpApplication.getAppContext();
        SharedPreferences prefs = context.getSharedPreferences(DATA_PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(DEVICE_NAME, mEditText.getText().toString());
        editor.apply();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_settings, null, false);

        TextView btnPositive = (TextView) dialogView.findViewById(R.id.txtvPositiveButton);
        TextView btnNegative = (TextView) dialogView.findViewById(R.id.txtvNegativeButton);
        mEditText = (EditText) dialogView.findViewById(R.id.editCode);

        loadData();

        btnPositive.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        saveData();
                        dismiss();
                    }
                }
        );

        btnNegative.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                }
        );

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        AlertDialog dialog = dialogBuilder.create();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        return dialog;
    }
}
