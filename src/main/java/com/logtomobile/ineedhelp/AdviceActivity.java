package com.logtomobile.ineedhelp;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import static com.logtomobile.ineedhelp.BundleConstants.ITEM_1;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_2;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_3;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_4;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_5;
import static com.logtomobile.ineedhelp.BundleConstants.ITEM_6;

/**
 *@author Tomasz Trybała
 */
public class AdviceActivity extends Activity{
    private WebView mWebView;

    private void findViews(){
        mWebView = (WebView) findViewById(R.id.webvArticle);
    }

    private void loadWebView(){
        if(getIntent().hasExtra(ITEM_1)){
            mWebView.loadUrl("file:///android_asset/personal_safety.html");
            setActionBarTitle(R.string.list_1);
        }
        if(getIntent().hasExtra(ITEM_2)){
            mWebView.loadUrl("file:///android_asset/safer_homes.html");
            setActionBarTitle(R.string.list_2);
        }
        if(getIntent().hasExtra(ITEM_3)){
            mWebView.loadUrl("file:///android_asset/safer_neighbourhoods.html");
            setActionBarTitle(R.string.list_3);
        }
        if(getIntent().hasExtra(ITEM_4)){
            mWebView.loadUrl("file:///android_asset/fire_safety.html");
            setActionBarTitle(R.string.list_4);
        }
        if(getIntent().hasExtra(ITEM_5)){
            mWebView.loadUrl("file:///android_asset/internet_safety.html");
            setActionBarTitle(R.string.list_5);
        }
        if(getIntent().hasExtra(ITEM_6)){
            mWebView.loadUrl("file:///android_asset/products_and_services.html");
            setActionBarTitle(R.string.list_6);
        }
    }

    private void setActionBarTitle(int text){
        if(getActionBar() != null){
            getActionBar().setTitle(getResources().getString(text));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advice);
        findViews();
        loadWebView();
    }
}
