package com.logtomobile.ineedhelp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import com.logtomobile.ineedhelp.service.INeedHelpProviderService;
import java.io.IOException;

/**
 * @author Tomasz Trybała
 */
public class DefineContactsActivity extends Activity {
    private static final String NAME_1 = "name1";
    private static final String PHONE_1 = "phone1";
    private static final String NAME_2 = "name2";
    private static final String PHONE_2 = "phone2";
    private static final String NAME_3 = "name3";
    private static final String PHONE_3 = "phone3";
    private static final String IS_CONTACTS = "contacts";
    private static final String DATA_PREFS = "dataPrefs";
    private static final String EMPTY_CONTACTS = "no_contacts";
    private static final String NOT_EMPTY_CONTACTS = "contacts";
    private static final int HOST_NUMBER = 105;
    private static final int REQUEST_CODE_PICK_CONTACTS_1 = 1;
    private static final int REQUEST_CODE_PICK_CONTACTS_2 = 2;
    private static final int REQUEST_CODE_PICK_CONTACTS_3 = 3;
    private Uri uriContact;
    private String contactID;
    private EditText mEdtName1;
    private EditText mEdtPhone1;
    private EditText mEdtName2;
    private EditText mEdtPhone2;
    private EditText mEdtName3;
    private EditText mEdtPhone3;
    private ImageButton mBtn1;
    private ImageButton mBtn2;
    private ImageButton mBtn3;

    private void findViews() {
        mEdtName1 = (EditText) findViewById(R.id.edt1Name);
        mEdtPhone1 = (EditText) findViewById(R.id.edt1Phone);
        mEdtName2 = (EditText) findViewById(R.id.edt2Name);
        mEdtPhone2 = (EditText) findViewById(R.id.edt2Phone);
        mEdtName3 = (EditText) findViewById(R.id.edt3Name);
        mEdtPhone3 = (EditText) findViewById(R.id.edt3Phone);

        mBtn1 = (ImageButton) findViewById(R.id.btnContacts1);
        mBtn2 = (ImageButton) findViewById(R.id.btnContacts2);
        mBtn3 = (ImageButton) findViewById(R.id.btnContacts3);
    }

    private void saveData() {
        Context context = INeedHelpApplication.getAppContext();
        SharedPreferences prefs = context.getSharedPreferences(DATA_PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(NAME_1, mEdtName1.getText().toString());
        editor.putString(PHONE_1, mEdtPhone1.getText().toString());
        editor.putString(NAME_2, mEdtName2.getText().toString());
        editor.putString(PHONE_2, mEdtPhone2.getText().toString());
        editor.putString(NAME_3, mEdtName3.getText().toString());
        editor.putString(PHONE_3, mEdtPhone3.getText().toString());


        boolean safetyContact1 =
                !mEdtPhone1.getText().toString().equals("");
        boolean safetyContact2 =
                !mEdtPhone2.getText().toString().equals("");
        boolean safetyContact3 =
                !mEdtPhone3.getText().toString().equals("");
        if (safetyContact1 || safetyContact2 || safetyContact3) {
            editor.putBoolean(IS_CONTACTS, true);
            sendMessage(NOT_EMPTY_CONTACTS);
        } else {
            editor.putBoolean(IS_CONTACTS, false);
            sendMessage(EMPTY_CONTACTS);
        }

        editor.apply();
    }

    private void sendMessage(final String text) {
        if(INeedHelpApplication.getMap() != null) {
            final INeedHelpProviderService.HelloAccessoryProviderConnection uHandler = INeedHelpApplication.getMap().get(Integer
                    .parseInt(String.valueOf(INeedHelpApplication.getId())));
            if (uHandler == null) {
                return;
            }
            new Thread(new Runnable() {
                public void run() {
                    try {
                        uHandler.send(HOST_NUMBER, text.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private void loadData() {
        Context context = INeedHelpApplication.getAppContext();
        SharedPreferences prefs = context.getSharedPreferences(DATA_PREFS, 0);

        String name1 = prefs.getString(NAME_1, "");
        if (!name1.equals("")) {
            mEdtName1.setText(name1);
        }

        String name2 = prefs.getString(NAME_2, "");
        if (!name2.equals("")) {
            mEdtName2.setText(name2);
        }

        String name3 = prefs.getString(NAME_3, "");
        if (!name3.equals("")) {
            mEdtName3.setText(name3);
        }

        String phone1 = prefs.getString(PHONE_1, "");
        if (!phone1.equals("")) {
            mEdtPhone1.setText(phone1);
        }

        String phone2 = prefs.getString(PHONE_2, "");
        if (!phone2.equals("")) {
            mEdtPhone2.setText(phone2);
        }

        String phone3 = prefs.getString(PHONE_3, "");
        if (!phone3.equals("")) {
            mEdtPhone3.setText(phone3);
        }
    }

    private void setButtonsAction(){
        mBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                        ContactsContract.Contacts.CONTENT_URI),
                        REQUEST_CODE_PICK_CONTACTS_1);
            }
        });

        mBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                        ContactsContract.Contacts.CONTENT_URI),
                        REQUEST_CODE_PICK_CONTACTS_2);
            }
        });

        mBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                        ContactsContract.Contacts.CONTENT_URI),
                        REQUEST_CODE_PICK_CONTACTS_3);
            }
        });
    }

    private void retrieveContactNumber(final EditText edtNumber) {

        String contactNumber = null;

        Cursor cursorID = getContentResolver().query(uriContact,
                new String[]{ContactsContract.Contacts._ID},
                null, null, null);

        if (cursorID.moveToFirst()) {
            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
        }

        cursorID.close();

        Cursor cursorPhone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                new String[]{contactID},
                null);

        if (cursorPhone.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }

        cursorPhone.close();
        Handler handler = new Handler(getMainLooper());
        final String finalContactNumber = contactNumber;
        handler.post(new Runnable() {
            @Override
            public void run() {
                edtNumber.setText(finalContactNumber);
            }
        });
    }

    private void retrieveContactName(final EditText edtName) {

        String contactName = null;

        Cursor cursor = getContentResolver().query(uriContact, null, null, null, null);

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        }

        cursor.close();
        Handler handler = new Handler(getMainLooper());
        final String finalContactName = contactName;
        handler.post(new Runnable() {
            @Override
            public void run() {
                edtName.setText(finalContactName);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_define_contacts);
        findViews();
        setButtonsAction();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            uriContact = data.getData();
            switch (requestCode){
                case REQUEST_CODE_PICK_CONTACTS_1:
                    retrieveContactName(mEdtName1);
                    retrieveContactNumber(mEdtPhone1);
                    break;
                case REQUEST_CODE_PICK_CONTACTS_2:
                    retrieveContactName(mEdtName2);
                    retrieveContactNumber(mEdtPhone2);
                    break;
                case REQUEST_CODE_PICK_CONTACTS_3:
                    retrieveContactName(mEdtName3);
                    retrieveContactNumber(mEdtPhone3);
                    break;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveData();
        INeedHelpApplication.getApplicationEventBus().unregister(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
        INeedHelpApplication.getApplicationEventBus().register(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menuSave:
                finish();
                break;
        }

        return super.onOptionsItemSelected(menuItem);
    }
}
