package com.logtomobile.ineedhelp;

import android.app.Activity;
import android.media.AudioManager;
import android.os.Bundle;

/**
 *@author Tomasz Trybała
 */
public class AboutActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_section);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }
}
