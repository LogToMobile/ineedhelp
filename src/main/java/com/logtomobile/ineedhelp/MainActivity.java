package com.logtomobile.ineedhelp;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.common.eventbus.Subscribe;
import com.logtomobile.ineedhelp.event.ConnectionEvent;
import com.logtomobile.ineedhelp.service.SendService;
import com.logtomobile.ineedhelp.service.SendService.MyLocalBinder;


public class MainActivity extends Activity {
//    private static final String DATA_PREFS = "dataPrefs";
//    private static final String PHONE_1 = "phone1";
//    private static final String PHONE_2 = "phone2";
//    private static final String PHONE_3 = "phone3";
//    private static final String IS_CONTACTS = "contacts";

    private boolean mDoubleBackToExitPressedOnce;
//    private ImageView mBtn1;
//    private ImageView mBtn2;
//    private ImageView mBtn3;
    private ImageView mBtn4;
//    private ImageView mBtnSend;
    private boolean mBound;
//    private double mLatitude;
//    private double mLongitude;
    private ServiceConnection myConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            MyLocalBinder binder = (MyLocalBinder) iBinder;
            SendService mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
        }
    };

    private void findViews() {
//        mBtn1 = (ImageView) findViewById(R.id.btn1);
//        mBtn2 = (ImageView) findViewById(R.id.btn2);
//        mBtn3 = (ImageView) findViewById(R.id.btn3);
//        mBtnSend = (ImageView) findViewById(R.id.btnSend);
        mBtn4 = (ImageView) findViewById(R.id.btn4);
    }

    private void setLabel() {
        if (getActionBar() != null) {
            getActionBar().setTitle(getResources().getString(R.string.main_title));
        }
    }

    private void setButtonsAction() {
//        mBtn1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, DefineContactsActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        mBtn2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, SafetyAdvicesActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        mBtnSend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Context context = INeedHelpApplication.getAppContext();
//                SharedPreferences prefs = context.getSharedPreferences(DATA_PREFS, 0);
//                if (prefs.getBoolean(IS_CONTACTS, false)) {
//                    setContacts(generateMessage("" + mLatitude,
//                            "" + mLongitude));
//                } else {
//                    Toast.makeText(context, getString(R.string.no_safety_contacts), Toast.LENGTH_LONG).show();
//                }
//            }
//        });

//        mBtn3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, ExampleActivity.class);
//                startActivity(intent);
//            }
//        });

        mBtn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AmbulanceActivity.class);
                startActivity(intent);
            }
        });
    }

//    private String generateMessage(String latitude, String longtitude) {
//        return getString(R.string.message_first) + latitude + "," + longtitude +
//                getString(R.string.message_second) + latitude + "," + longtitude + getString(R.string.message_third);
//    }

//    private void setContacts(String message) {
//        Context context = INeedHelpApplication.getAppContext();
//        SharedPreferences prefs = context.getSharedPreferences(DATA_PREFS, 0);
//
//        String phone1 = prefs.getString(PHONE_1, "");
//        if (!phone1.equals("")) {
//            Toast.makeText(getBaseContext(), getString(R.string.sending),
//                    Toast.LENGTH_SHORT).show();
//            sendSMS(phone1, message);
//        }
//
//        String phone2 = prefs.getString(PHONE_2, "");
//        if (!phone2.equals("")) {
//            Toast.makeText(getBaseContext(), getString(R.string.sending),
//                    Toast.LENGTH_SHORT).show();
//            sendSMS(phone2, message);
//        }
//
//        String phone3 = prefs.getString(PHONE_3, "");
//        if (!phone3.equals("")) {
//            Toast.makeText(getBaseContext(), getString(R.string.sending),
//                    Toast.LENGTH_SHORT).show();
//            sendSMS(phone3, message);
//        }
//    }

//    private void sendSMS(String phoneNumber, String message) {
//        String SENT = "SMS_SENT";
//        String DELIVERED = "SMS_DELIVERED";
//
//        registerReceiver(new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//
//                switch (getResultCode()) {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getBaseContext(), getString(R.string.sms_success),
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                        Toast.makeText(getBaseContext(), getString(R.string.error_generic_failur),
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NO_SERVICE:
//                        Toast.makeText(getBaseContext(), getString(R.string.error_no_service),
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NULL_PDU:
//                        Toast.makeText(getBaseContext(), getString(R.string.error_no_pdu),
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_RADIO_OFF:
//                        Toast.makeText(getBaseContext(), getString(R.string.error_radio_off),
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    default:
//                }
//                unregisterReceiver(this);
//            }
//        }, new IntentFilter(SENT));
//
//        SmsManager sms = SmsManager.getDefault();
//
//        PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
//        PendingIntent deliveryIntent = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
//        sms.sendTextMessage(phoneNumber, null, message, sentIntent, deliveryIntent);
//    }

//    private void setLocationListener(){
//        LocationListener locationListener = new LocationListener() {
//            @Override
//            public void onLocationChanged(Location location) {
//                mLatitude = location.getLatitude();
//                mLongitude = location.getLongitude();
//            }
//
//            @Override
//            public void onStatusChanged(String s, int i, Bundle bundle) {
//            }
//
//            @Override
//            public void onProviderEnabled(String s) {
//            }
//
//            @Override
//            public void onProviderDisabled(String s) {
//            }
//        };
//
//        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        setLabel();
        setButtonsAction();
//        setLocationListener();
    }

    @Override
    public void onStop() {
        INeedHelpApplication.getApplicationEventBus().unregister(this);
        super.onStop();
        if (mBound) {
            unbindService(myConnection);
            mBound = false;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        INeedHelpApplication.getApplicationEventBus().register(this);
        Intent intent = new Intent(this, SendService.class);
        bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onBackPressed() {
        if (mDoubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.mDoubleBackToExitPressedOnce = true;
        Toast.makeText(MainActivity.this, getResources().getString(R.string.doubleBackToExitText), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mDoubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menuActionSettings:
                SettingsDialogFragment dialog = new SettingsDialogFragment();
                dialog.show(getFragmentManager(), "settings code dialog");
                break;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @SuppressWarnings("UnusedDeclaration")
    @Subscribe
    public void setConnection(final ConnectionEvent event) {
        Handler handler = new Handler(getMainLooper());
        if (event.getConnection()) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (getBaseContext() != null) {
                        Toast.makeText(getBaseContext(),
                                getString(R.string.connected), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (getBaseContext() != null) {
                        Toast.makeText(getBaseContext(),
                                getString(R.string.disconnected), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
